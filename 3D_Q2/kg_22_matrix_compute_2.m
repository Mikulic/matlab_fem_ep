function [KGTOTAL] = kg_22_matrix_compute_2(B, detJ, CC,n_int_tot, elem, n_dof)

KGTOTAL=sparse(n_dof,n_dof);
DIAG=sparse(n_dof,1);

inodes=kron(1:20,[1 1 1]);
icomps=repmat([1 2 3],1,20);

E=cell(6,60); [E{:,:}]=deal(zeros(n_int_tot,1));

for i=1:6
    for j=1:60
        E{i,j}=CC{i,1}.*B{1,j}+CC{i,2}.*B{2,j}+CC{i,3}.*B{3,j}+ ... 
               CC{i,4}.*B{4,j}+CC{i,5}.*B{5,j}+CC{i,6}.*B{6,j};
    end
end

for i=1:60
    k=sparse(n_dof,n_dof);
    ik=3*(inodes(i)-1)+icomps(i); it=3*(elem(inodes(i),:)-1)+icomps(i);
    for j=1:i-1
        jl=3*(inodes(j)-1)+icomps(j); jt=3*(elem(inodes(j),:)-1)+icomps(j);
        %Kij = h*detJ'.*(B{1,i}.*E{1,j}+B{2,i}.*E{2,j}+B{3,i}.*E{3,j});
         Kij = detJ'.*(B{1,i}.*E{1,j}+B{2,i}.*E{2,j}+B{3,i}.*E{3,j} + ...
                         B{4,i}.*E{4,j}+B{5,i}.*E{5,j}+B{6,i}.*E{6,j});
        %Kij = sum(reshape(Kij,4,[]),1);
        %KGTOTAL=KGTOTAL+sparse(it,jt,sum(reshape(Kij,8,[]),1),n_dof,n_dof);
        k=k+sparse(it,jt,sum(reshape(Kij,27,[]),1),n_dof,n_dof);
    end
    %Kii = h*detJ'.*(B{1,i}.*E{1,i}+B{2,i}.*E{2,i}+B{3,i}.*E{3,i});
    Kii = detJ'.*(B{1,i}.*E{1,i}+B{2,i}.*E{2,i}+B{3,i}.*E{3,i} + ...
                  B{4,i}.*E{4,i}+B{5,i}.*E{5,i}+B{6,i}.*E{6,i});
    %Kii = sum(reshape(Kii,4,[]),1);
    DIAG=DIAG+sparse(it,1,sum(reshape(Kii,27,[]),1),n_dof,1);
    KGTOTAL = KGTOTAL + k;
    clear k;
end

KGTOTAL=KGTOTAL+KGTOTAL.'+diag(DIAG);


end