function [B, detJW] = b_matrix_compute_3D_Q2(xx, yy, zz, n_el)

% ======================================================================
%
% Strain displacement matrix B
%
%    output: 
%      B - strain displacement matrix at integration point (xi, eta)
%      detJ - determinant of Jacobian
%                  
%    input:
%      xi, eta  - coordinates of Gauss integration point
%      xx, yy   - coordinates of elements nodes                         
%      nel - number of elements
%
% =========================================================================

    %
    % number of nodes per element, number of integration points per
    % elementnt
    %
    
    n_n_el=20; n_int_p_el=27;

    %
    % total number of integrations points
    %
    
    n_int_tot = n_el*n_int_p_el;

    %
    % Gauss points
    %
    % - the reference cube with coordinates:
    %         [-1,-1,-1], [1,-1,-1], [1,1,-1], [-1,1,-1], 
    %          [-1,-1,1],  [1,-1,1],  [1,1,1],  [-1,1,1]
    % - (3x3x3)-point quadrature rule, i.e., n_q=27    
    p = sqrt(3/5);
    gP=[-p,-p,-p,-p,-p,-p,-p,-p,-p,  0,  0,  0,  0,  0,  0,  0,  0,  0, p, p, p, p, p, p, p, p, p
        -p,-p,-p,  0,  0,  0, p, p, p,-p,-p,-p,  0,  0,  0, p, p, p,-p,-p,-p,  0,  0,  0, p, p, p
        -p,  0, p,-p,  0, p,-p,  0, p,-p,  0, p,-p,  0, p,-p,  0, p,-p,  0, p,-p,  0, p,-p,  0, p];
    
    %
    % Gauss coefficients
    %
    
    gW = ((5/9)^3)* [1 0 1 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 1 0 1 0 0 0 1 0 1] +...
     ((5/9)^2)*(8/9)* [0 1 0 1 0 1 0 1 0 1 0 1 0 0 0 1 0 1 0 1 0 1 0 1 0 1 0] +...     
     ((8/9)^2)*(5/9)* [0 0 0 0 1 0 0 0 0 0 1 0 1 0 1 0 1 0 0 0 0 0 1 0 0 0 0] +...
           ((8/9)^3)* [0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0];
    
    xi = gP(1,:); eta = gP(2,:); zed=gP(3,:);

    %
    % coordinates of element nodes for integration points
    %
    
    xx_int=kron(xx,ones(1,n_int_p_el)); 
    yy_int=kron(yy,ones(1,n_int_p_el));
    zz_int=kron(zz,ones(1,n_int_p_el));

    %
    % Shape functions derivative (to xi eta zed)
    %
    % - the reference cube with coordinates:
    %         [-1,-1,-1], [1,-1,-1], [1,1,-1], [-1,1,-1], 
    %          [-1,-1,1],  [1,-1,1],  [1,1,1],  [-1,1,1]
    % - coordinates of midpoints:
    %          [0,-1,-1], [1,0,-1],  [0,1,-1], [-1,0,-1], [1,0,1]
    %            [0,1,1], [-1,0,1], [-1,-1,0],  [1,-1,0], [-1,1,0]
    % - n_p=20, n_q=length(xi) 


    Nd_xi = [ (1-eta).*(1-zed).*( 1+2*xi+eta+zed)/8;
               (1-eta).*(1-zed).*(-1+2*xi-eta-zed)/8;
               (1+eta).*(1-zed).*(-1+2*xi+eta-zed)/8;
               (1+eta).*(1-zed).*( 1+2*xi-eta+zed)/8;
               (1-eta).*(1+zed).*( 1+2*xi+eta-zed)/8;
               (1-eta).*(1+zed).*(-1+2*xi-eta+zed)/8;
               (1+eta).*(1+zed).*(-1+2*xi+eta+zed)/8;
               (1+eta).*(1+zed).*( 1+2*xi-eta-zed)/8;
                               -xi.*(1-eta).*(1-zed)/2;
                                   (1-eta.^2).*(1-zed)/4;
                               -xi.*(1+eta).*(1-zed)/2;
                                  -(1-eta.^2).*(1-zed)/4;
                               -xi.*(1-eta).*(1+zed)/2;
                                   (1-eta.^2).*(1+zed)/4;
                               -xi.*(1+eta).*(1+zed)/2;
                                  -(1-eta.^2).*(1+zed)/4;
                                  -(1-eta).*(1-zed.^2)/4;
                                   (1-eta).*(1-zed.^2)/4;
                                   (1+eta).*(1-zed.^2)/4;
                                  -(1+eta).*(1-zed.^2)/4 ];

    Nd_eta = [ (1-xi).*(1-zed).*( 1+xi+2*eta+zed)/8;
               (1+xi).*(1-zed).*( 1-xi+2*eta+zed)/8;
               (1+xi).*(1-zed).*(-1+xi+2*eta-zed)/8;
               (1-xi).*(1-zed).*(-1-xi+2*eta-zed)/8;
               (1-xi).*(1+zed).*( 1+xi+2*eta-zed)/8;
               (1+xi).*(1+zed).*( 1-xi+2*eta-zed)/8;
               (1+xi).*(1+zed).*(-1+xi+2*eta+zed)/8;
               (1-xi).*(1+zed).*(-1-xi+2*eta+zed)/8;
                                  -(1-xi.^2).*(1-zed)/4;
                               -(1+xi).*eta.*(1-zed)/2;
                                   (1-xi.^2).*(1-zed)/4;
                               -(1-xi).*eta.*(1-zed)/2;
                                  -(1-xi.^2).*(1+zed)/4;
                               -(1+xi).*eta.*(1+zed)/2;
                                   (1-xi.^2).*(1+zed)/4;
                               -(1-xi).*eta.*(1+zed)/2;
                                  -(1-xi).*(1-zed.^2)/4;
                                  -(1+xi).*(1-zed.^2)/4;
                                   (1+xi).*(1-zed.^2)/4;
                                   (1-xi).*(1-zed.^2)/4 ];

    Nd_zed = [ (1-xi).*(1-eta).*( 1+xi+eta+2*zed)/8;
               (1+xi).*(1-eta).*( 1-xi+eta+2*zed)/8;
               (1+xi).*(1+eta).*( 1-xi-eta+2*zed)/8;
               (1-xi).*(1+eta).*( 1+xi-eta+2*zed)/8;
               (1-xi).*(1-eta).*(-1-xi-eta+2*zed)/8;
               (1+xi).*(1-eta).*(-1+xi-eta+2*zed)/8;
               (1+xi).*(1+eta).*(-1+xi+eta+2*zed)/8;
               (1-xi).*(1+eta).*(-1-xi+eta+2*zed)/8;
                                  -(1-xi.^2).*(1-eta)/4;
                                  -(1+xi).*(1-eta.^2)/4;
                                  -(1-xi.^2).*(1+eta)/4;
                                  -(1-xi).*(1-eta.^2)/4;
                                   (1-xi.^2).*(1-eta)/4;
                                   (1+xi).*(1-eta.^2)/4;
                                   (1-xi.^2).*(1+eta)/4;
                                   (1-xi).*(1-eta.^2)/4;
                               -(1-xi).*(1-eta).*zed/2;
                               -(1+xi).*(1-eta).*zed/2;
                               -(1+xi).*(1+eta).*zed/2;
                               -(1-xi).*(1+eta).*zed/2 ];    

    %
    % Shape functions derivative for all elements
    %
    
    Nd_xi_all_el=repmat(Nd_xi,1,n_el);
    Nd_eta_all_el=repmat(Nd_eta,1,n_el);
    Nd_zed_all_el=repmat(Nd_zed,1,n_el);
    
    % components of the Jacobians: size=(1,n_int)
    J11=sum(xx_int.*Nd_xi_all_el); 
    J12=sum(yy_int.*Nd_xi_all_el); 
    J13=sum(zz_int.*Nd_xi_all_el);

    J21=sum(xx_int.*Nd_eta_all_el); 
    J22=sum(yy_int.*Nd_eta_all_el);
    J23=sum(zz_int.*Nd_eta_all_el);
    
    J31=sum(xx_int.*Nd_zed_all_el); 
    J32=sum(yy_int.*Nd_zed_all_el);
    J33=sum(zz_int.*Nd_zed_all_el); 


    
    % determinant of the Jacobian: size=(1,n_int)
    detJ=J11.*(J22.*J33-J32.*J23) - J12.*(J21.*J33-J23.*J31) + J13.*(J21.*J32-J22.*J31);

    % determinant of the Jacobian multiplied with gW
    detJW=detJ.*repmat(gW,1,n_el);
    
    % components of the inverse to the Jacobian: size=(1,n_int)
    Jinv11 =  (J22.*J33-J23.*J32)./detJ; 
    Jinv12 = -(J12.*J33-J13.*J32)./detJ; 
    Jinv13 =  (J12.*J23-J13.*J22)./detJ;

    Jinv21 = -(J21.*J33-J23.*J31)./detJ; 
    Jinv22 =  (J11.*J33-J13.*J31)./detJ; 
    Jinv23 = -(J11.*J23-J13.*J21)./detJ; 
    Jinv31 =  (J21.*J32-J22.*J31)./detJ; 
    Jinv32 = -(J11.*J32-J12.*J31)./detJ; 
    Jinv33 =  (J11.*J22-J12.*J21)./detJ;
    
    % Shape functions derivative (to x y z)

    Nd_x = repmat(Jinv11,n_n_el,1).*Nd_xi_all_el + ...
           repmat(Jinv12,n_n_el,1).*Nd_eta_all_el + ...
           repmat(Jinv13,n_n_el,1).*Nd_zed_all_el;

    Nd_y = repmat(Jinv21,n_n_el,1).*Nd_xi_all_el + ...
           repmat(Jinv22,n_n_el,1).*Nd_eta_all_el + ...
           repmat(Jinv23,n_n_el,1).*Nd_zed_all_el;

    Nd_z = repmat(Jinv31,n_n_el,1).*Nd_xi_all_el + ...
           repmat(Jinv32,n_n_el,1).*Nd_eta_all_el + ...
           repmat(Jinv33,n_n_el,1).*Nd_zed_all_el;
    
    Nd_x_cell=cell(1,20); Nd_y_cell=cell(1,20); Nd_z_cell=cell(1,20); 
    
    for i=1:20
        Nd_x_cell{i}=Nd_x(i,:)';
        Nd_y_cell{i}=Nd_y(i,:)';
        Nd_z_cell{i}=Nd_z(i,:)';
    end  
    
    B=cell(6,60); [B{:,:}]=deal(sparse(n_int_tot,1));

    [B{1,1:3:58}]=deal(Nd_x_cell{:});
    [B{2,2:3:59}]=deal(Nd_y_cell{:});
    [B{3,3:3:60}]=deal(Nd_z_cell{:});
    [B{4,1:3:58}]=deal(Nd_y_cell{:});
    [B{4,2:3:59}]=deal(Nd_x_cell{:});
    [B{5,2:3:59}]=deal(Nd_z_cell{:});
    [B{5,3:3:60}]=deal(Nd_y_cell{:});
    [B{6,1:3:58}]=deal(Nd_z_cell{:});
    [B{6,3:3:60}]=deal(Nd_x_cell{:});
    
    

end
