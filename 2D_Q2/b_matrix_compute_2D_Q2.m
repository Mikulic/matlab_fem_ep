function [B, detJW] = b_matrix_compute_2D_Q2(xx, yy, n_el)

% ======================================================================
%
% Strain displacement matrix B
%
%    output: 
%      B - strain displacement matrix at integration point (xi, eta)
%      detJ - determinant of Jacobian
%                  
%    input:
%      xi, eta  - coordinates of Gauss integration point
%      xx, yy   - coordinates of elements nodes                         
%      nel - number of elements
%
% =========================================================================

    %
    % number of nodes per element, number of integration points per
    % elementnt
    %
    
    n_n_el=8; n_int_p_el=9;

    %
    % total number of integrations points
    %
    
    n_int_tot = n_el*n_int_p_el;

    %
    % Gauss points
    %
    
    p = sqrt(3/5);
    gP = [-p -p; p -p; p p; -p p; 0 -p; p 0; 0 p; -p 0; 0 0]';
    xi = gP(1,:); eta = gP(2,:);

    %
    % Gauss coefficients
    %
    
    gW = [25/81 25/81 25/81 25/81 40/81 40/81 40/81 40/81 64/81];
 
    %
    % coordinates of element nodes for integration points
    %
    
    xx_int=kron(xx,ones(1,n_int_p_el)); 
    yy_int=kron(yy,ones(1,n_int_p_el));

    %
    % Shape functions derivative (to xi eta)
    %

    Nd_xi = [ (1-eta).*(2*xi+eta)/4;
              (1-eta).*(2*xi-eta)/4;
              (1+eta).*(2*xi+eta)/4;
              (1+eta).*(2*xi-eta)/4;             
              -xi.*(1-eta);
              (1-eta.^2)/2;
              -xi.*(1+eta);
              -(1-eta.^2)/2 ];

    Nd_eta = [ (1-xi).*( xi+2*eta)/4;
               (1+xi).*(-xi+2*eta)/4;
               (1+xi).*( xi+2*eta)/4;
               (1-xi).*(-xi+2*eta)/4;             
               -(1-xi.^2)/2;
               -(1+xi).*eta;
               (1-xi.^2)/2;
               -(1-xi).*eta; ];

    %
    % Shape functions derivative for all elements
    %
    
    Nd_xi_all_el=repmat(Nd_xi,1,n_el);
    Nd_eta_all_el=repmat(Nd_eta,1,n_el);
    
    % components of the Jacobians: size=(1,n_int)
    J11=sum(xx_int.*Nd_xi_all_el); J12=sum(yy_int.*Nd_xi_all_el); 
    J21=sum(xx_int.*Nd_eta_all_el); J22=sum(yy_int.*Nd_eta_all_el); 
    
    % determinant of the Jacobian: size=(1,n_int)
    detJ=J11.*J22 - J12.*J21;

    % determinant of the Jacobian multiplied with gW
    detJW=detJ.*repmat(gW,1,n_el);
    
    % components of the inverse to the Jacobian: size=(1,n_int)
    Jinv11 =  J22./detJ; Jinv12 = -J12./detJ;
    Jinv21 = -J21./detJ; Jinv22 =  J11./detJ;
    
    % Shape functions derivative (to x y)

    Nd_x = repmat(Jinv11,n_n_el,1).*Nd_xi_all_el + repmat(Jinv12,n_n_el,1).*Nd_eta_all_el;
    Nd_y = repmat(Jinv21,n_n_el,1).*Nd_xi_all_el + repmat(Jinv22,n_n_el,1).*Nd_eta_all_el;

    B=cell(3,16); [B{:,:}]=deal(sparse(n_int_tot,1));

    B{1,1}=Nd_x(1,:)'; B{1,3} = Nd_x(2,:)'; B{1,5} = Nd_x(3,:)'; B{1,7} = Nd_x(4,:)';
    B{1,9}=Nd_x(5,:)'; B{1,11} = Nd_x(6,:)'; B{1,13} = Nd_x(7,:)'; B{1,15} = Nd_x(8,:)';

    B{2,2}=Nd_y(1,:)'; B{2,4} = Nd_y(2,:)'; B{2,6} = Nd_y(3,:)'; B{2,8} = Nd_y(4,:)';
    B{2,10}=Nd_y(5,:)'; B{2,12} = Nd_y(6,:)'; B{2,14} = Nd_y(7,:)'; B{2,16} = Nd_y(8,:)';
    
    B{3,1}=Nd_y(1,:)'; B{3,3} = Nd_y(2,:)'; B{3,5} = Nd_y(3,:)'; B{3,7} = Nd_y(4,:)';
    B{3,9}=Nd_y(5,:)'; B{3,11} = Nd_y(6,:)'; B{3,13} = Nd_y(7,:)'; B{3,15} = Nd_y(8,:)';

    B{3,2}=Nd_x(1,:)'; B{3,4} = Nd_x(2,:)'; B{3,6} = Nd_x(3,:)'; B{3,8} = Nd_x(4,:)';
    B{3,10}=Nd_x(5,:)'; B{3,12} = Nd_x(6,:)'; B{3,14} = Nd_x(7,:)'; B{3,16} = Nd_x(8,:)';

end
