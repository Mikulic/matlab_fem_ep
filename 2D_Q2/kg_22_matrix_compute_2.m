function [KGTOTAL] = kg_22_matrix_compute_2(B, detJ, CC,n_int_tot, elem, n_dof)

KGTOTAL=sparse(n_dof,n_dof);
DIAG=sparse(n_dof,1);

inodes=kron(1:8,[1 1]);
icomps=repmat([1 2],1,8);

E=cell(3,16); [E{:,:}]=deal(zeros(n_int_tot,1));

for i=1:3
    for j=1:16
        E{i,j}=CC{i,1}.*B{1,j}+CC{i,2}.*B{2,j}+CC{i,3}.*B{3,j};
    end
end

for i=1:16
    k=sparse(n_dof,n_dof);
    ik=2*(inodes(i)-1)+icomps(i); it=2*(elem(inodes(i),:)-1)+icomps(i);
    for j=1:i-1
        %jl=3*(inodes(j)-1)+icomps(j); jt=3*(elem(inodes(j),:)-1)+icomps(j);
        jt=2*(elem(inodes(j),:)-1)+icomps(j);

         Kij = detJ'.*(B{1,i}.*E{1,j}+B{2,i}.*E{2,j}+B{3,i}.*E{3,j});

        k=k+sparse(it,jt,sum(reshape(Kij,9,[]),1),n_dof,n_dof);
    end
    %Kii = h*detJ'.*(B{1,i}.*E{1,i}+B{2,i}.*E{2,i}+B{3,i}.*E{3,i});
    Kii = detJ'.*(B{1,i}.*E{1,i}+B{2,i}.*E{2,i}+B{3,i}.*E{3,i});
    %Kii = sum(reshape(Kii,4,[]),1);
    DIAG=DIAG+sparse(it,1,sum(reshape(Kii,9,[]),1),n_dof,1);
    KGTOTAL = KGTOTAL + k;
    clear k;
end

KGTOTAL=KGTOTAL+KGTOTAL.'+diag(DIAG);


end