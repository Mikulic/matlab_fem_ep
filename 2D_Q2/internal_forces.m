function [FTOTAL] = internal_forces(B,S,detJW,n_int_tot,ndof,elements)

    FINT = zeros(n_int_tot,16); 

    for i=1:16
        FINT(:,i) = B{1,i}.*S(1,:)'+B{2,i}.*S(2,:)'+B{3,i}.*S(3,:)';
    end

    FTOTAL=sparse(ndof,1);
    inodes=[1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8]; 
    icomps=[1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2];

    for i=1:16
        ik=2*(inodes(i)-1)+icomps(i);it=2*(elements(inodes(i),:)-1)+icomps(i);
        FTOTAL=FTOTAL+sparse(it,1,sum(reshape(FINT(:,ik)'.*detJW,9,[]),1),ndof,1);
    end


end