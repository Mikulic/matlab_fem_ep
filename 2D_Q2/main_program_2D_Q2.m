
% =========================================================================
%
%  This is program for plain stress problem with isoparametric 
%  quadilateral element. 
%
% ======================================================================
%
clc; clear;

% geometrical parameters (choose only integers, size_hole < size_xy)
level=2;
size_xy = 10;      % size of the body in direction x and y 
size_hole = 5;     % size of the hole in the body
                 % the projection of the hole to the xy-plane is square 

P = 200;       % Load

[COORD,ELEM,SURF,NEUMANN,Q]=mesh_Q2(level,size_xy,size_hole);

coord=COORD;
elem=ELEM;

%
% thickness
%
h=1;

%
% number of elem, nodes and degrees of freedom(dof)
%
n_el=size(elem,2); n_nodes=size(coord,2); n_dof=2*n_nodes;

%
% number of nodes per element, number of integration points per elementn_int = n_e*n_q
%

n_n_el=8; n_int_p_el=9;

%
% total number of integrations points
%

n_int_tot = n_el*n_int_p_el ;

%
% coordinates of element nodes
%

xx=reshape(coord(1,elem(:,:)),n_n_el,n_el);
yy=reshape(coord(2,elem(:,:)),n_n_el,n_el);

%
% coordinates of element nodes for integration points
%

xx_int=kron(xx,ones(1,n_int_p_el)); 
yy_int=kron(yy,ones(1,n_int_p_el));

%
% element nodes in order
%

it1=elem(1,:); it2=elem(2,:); it3=elem(3,:); it4=elem(4,:);
it5=elem(5,:); it6=elem(6,:); it7=elem(7,:); it8=elem(8,:);

% values of elastic material parameters
young = 206900;                        % Young's modulus
poisson =  0.29;                       % Poisson's ratio
shear = young/(2*(1+poisson)) ;        % shear modulus
bulk = young/(3*(1-2*poisson)) ;       % bulk modulus

% values of elastic material parameters at elements
shear =shear*ones(1,n_int_tot);
bulk=bulk*ones(1,n_int_tot);

%
% constitutive matrix
%  

[C,CC] = c_matrix_compute_2D_Q2(n_int_tot);

%
% strain displacement matrix
% 

[BB,detJW] = b_matrix_compute_2D_Q2(xx, yy, n_el);

%
% Global stifness matrix
%

[KGTOTAL] = kg_22_matrix_compute_2(BB, detJW, CC,n_int_tot, elem, n_dof);

%
% Assembling of the vector of traction forces
%  

traction_force = [0, P] ;
traction_nodes = find(coord(2,:)==size_xy);

[segments] = traction_segments(coord, traction_nodes);

forces = traction_forces(segments,coord,traction_force);

%
% Dirichlet conditions
% 

dirichlet_nodesx0 = find(coord(1,:)==0);
dirichlet_nodesy0 = find(coord(2,:)==0);
dirichlet_dof = [2*dirichlet_nodesx0(1,:)-1 2*dirichlet_nodesy0(1,:)]';

dof=1:n_dof;
free_dof=setdiff(dof,dirichlet_dof);

%
% Loading process and Newton's solver
%

% values of plastic material parematers
Y=450*sqrt(2/3);

% number of load steps and values of load factors
d_zeta=1/10;              % constant load increment
zeta=[0:d_zeta:1, (1-d_zeta):-d_zeta:(-1), (-1+d_zeta):d_zeta:0];
                        % sequence of load factors
n_step = length(zeta);    % number of load steps
alpha=zeros(1,n_step);    % work of external forces  

% values of plastic material parematers at integration points
Y=Y*ones(1,n_int_tot);
% 
% initialization
U = zeros(2,n_nodes);
dU = zeros(2,n_nodes) ; % Newton's increment (in displacement)
U_old = zeros(2,n_nodes) ; 
F = zeros(2,n_nodes) ;  % vector of internal forces
E = zeros(3,n_int_tot); % strain tensors at integration points
Ep_old = zeros(4,n_int_tot); % plastic strain tensors at integration points

tic

% for loop through load steps 
for i=2:11 %n_step

    fprintf('step =%d ',i);
    fprintf('\n'); 
    
    f=zeta(i)*forces;     % the load vector at step i

    % initial displacements
    U_it=U;

    % Newton's solver (the semismooth Newton method)
    it=0;              % iteration number
    while true

      % strain at elements
      %E(:) = B*U_it(:);
      u1=U_it(1,:); u2=U_it(2,:);
      u=[u1(it1); u2(it1); u1(it2); u2(it2); u1(it3); u2(it3); u1(it4); u2(it4); ... 
         u1(it5); u2(it5); u1(it6); u2(it6); u1(it7); u2(it7); u1(it8); u2(it8) ]';
      uu=kron(u,ones(n_int_p_el,1));
      [E] = eps_compute_pl_2(BB, uu, n_int_tot);

      % solution of the constitutive problem
      [S,DS,IND_p]=constitutive_problem_2D(E,Ep_old,shear,bulk,Y);
      fprintf('  plastic integration points: %d , ',sum(IND_p)); 
      fprintf('\n');
      CCP=mat2cell(reshape(DS',n_int_tot*3,3),[n_int_tot n_int_tot n_int_tot],[1 1 1]);
      CDIF=cellfun(@minus,CCP,CC,'Un',0);

      n_ind_p=sum(IND_p);
      if n_ind_p>0

      BBB=cellfun(@(x) x(IND_p),BB,'UniformOutput', false);
      CDIF=cellfun(@(x) x(IND_p),CDIF,'UniformOutput', false);

      [KGTOTAL2] = kg_22_matrix_compute_2_work_Q2(BBB, detJW(IND_p), CDIF,n_int_tot, ...
                                               elem, n_dof, IND_p);

      %K_tangent = K_elast+B'*(D_p-D_elast)*B;
      KTANGENT=KGTOTAL + KGTOTAL2;
      else
      KTANGENT=KGTOTAL;
      end

      % vector of internal forces
      %F(:) = B'*reshape(repmat(WEIGHT,3,1).*S(1:3,:), 3*n_int,1);
      [FTOTAL] = internal_forces(BB,S,detJW,n_int_tot,n_dof,elem);

      % Newton's increment
      dU(free_dof) = KTANGENT(free_dof,free_dof)\(f(free_dof)'-FTOTAL(free_dof));

      % next iteration
      U_new= U_it + dU;

      % stopping criterion 
      q1 = sqrt( dU(:)'*KGTOTAL*dU(:) ) ;
      q2 = sqrt(  U_it(:)'*KGTOTAL*U_it(:)  ) ;
      q3 = sqrt( U_new(:)'*KGTOTAL*U_new(:) ) ;
      criterion = q1/(q2+q3);

      % update of unknown arrays
      U_it=U_new;                                                     
            
      % test on the stopping criterion
      if  criterion < 1e-12
          break
      end
        
      % test on number of iteration
      it=it+1; 
      if  it > 50
          error('The Newton solver does not converge.')
      end 
    end %  true

    U_old=U;
    U=U_it;

    u1=U(1,:); u2=U(2,:);
    u=[u1(it1); u2(it1); u1(it2); u2(it2); u1(it3); u2(it3); u1(it4); u2(it4); ... 
       u1(it5); u2(it5); u1(it6); u2(it6); u1(it7); u2(it7); u1(it8); u2(it8) ]';
    uu=kron(u,ones(n_int_p_el,1));
    [E] = eps_compute_pl_2(BB, uu, n_int_tot);
    [S,DS,IND_p,Ep]=constitutive_problem_2D(E,Ep_old,shear,bulk,Y); 
    Ep_old=Ep;
    alpha(i)=forces(free_dof)*U(free_dof)';

end

toc



