function [C,CC] = c_matrix_compute_2D_Q1(n_int_tot)

% ======================================================================
%
% Constitutive matrix computation
%
%    output: 
%      C - constitutive matrix
%                  
% =========================================================================


    E = 206900;                        % Youngs modulus
    nu = 0.29;                         % Poisson's ratio
    C = (E/((1+nu)*(1-2*nu)))*[1-nu nu 0 ; nu 1-nu 0; 0 0 (1-2*nu)/2];
    C1=(E/((1+nu)*(1-2*nu)))*ones(n_int_tot,1);
    CC=cell(3,3); [CC{:,:}]=deal(sparse(n_int_tot,1));
    CC{1,1}=C1*(1-nu); CC{2,2}=C1*(1-nu);
    CC{1,2}=C1*nu; CC{2,1}=C1*nu;
    CC{3,3}=C1*(1-2*nu)/2;

end