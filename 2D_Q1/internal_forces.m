function [FTOTAL] = internal_forces(B,S,detJW,n_int_tot,ndof,elements)

    FINT = zeros(n_int_tot,8); 

    for i=1:8
        FINT(:,i) = B{1,i}.*S(1,:)'+B{2,i}.*S(2,:)'+B{3,i}.*S(3,:)';
    end

    FTOTAL=sparse(ndof,1);
    inodes=kron(1:4,[1 1]);
    icomps=repmat([1 2],1,4);

    for i=1:8
        ik=2*(inodes(i)-1)+icomps(i);it=2*(elements(inodes(i),:)-1)+icomps(i);
        FTOTAL=FTOTAL+sparse(it,1,sum(reshape(FINT(:,ik)'.*detJW,4,[]),1),ndof,1);
    end


end