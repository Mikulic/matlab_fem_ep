function [eps] = eps_compute_pl_2(B, u, nel)

% ======================================================================
%
% Extrapolation of stresses from integration points of each
% element to nodes
%
%    output: 
%      s_extra - values of stresses at nodes
%                  
%    input:
%      elements - element nodes numbers                        
%      xx, yy - coordinates of elements nodes
%      u - 
%      nel - number of elements
%      C - constitutive matrix
%      gP - coordinates of Gauss integration points
%
% =========================================================================

    e411 = zeros(nel,1);e422 = zeros(nel,1);e412 = zeros(nel,1); 
    %

    for i=1:8                              
        e411(:) = e411(:) + B{1,i}.*u(:,i);
        e422(:) = e422(:) + B{2,i}.*u(:,i);
        e412(:) = e412(:) + B{3,i}.*u(:,i);
    end

    eps=[e411 e422 e412]';

end