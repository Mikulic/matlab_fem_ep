function [B, DET] = b_matrix_compute_2D_Q1(xx, yy, n_el)

% ======================================================================
%
% Strain displacement matrix B
%
%    output: 
%      B - strain displacement matrix at integration point (xi, eta)
%      detJ - determinant of Jacobian
%                  
%    input:
%      xi, eta  - coordinates of Gauss integration point
%      xx, yy   - coordinates of elements nodes                         
%      nel - number of elements
%
% =========================================================================

    %
    % number of nodes per element, number of integration points per
    % elementnt
    %
    
    n_n_el=4; n_int_p_el=4;

    %
    % total number of integrations points
    %
    
    n_int_tot = n_el*n_int_p_el;

    %
    % Gauss points
    %
    
    gP = [-1,-1; 1,-1; 1,1; -1,1]'*sqrt(3)/3;
    xi = gP(1,:); eta = gP(2,:);

    %
    % coordinates of element nodes for integration points
    %
    
    xx_int=kron(xx,ones(1,n_int_p_el)); 
    yy_int=kron(yy,ones(1,n_int_p_el));

    %
    % Shape functions derivative
    %
    
    Nd_xi = [eta/4 - 1/4; 1/4 - eta/4; eta/4 + 1/4; - eta/4 - 1/4];
    Nd_eta = [xi/4 - 1/4; - xi/4 - 1/4; xi/4 + 1/4; 1/4 - xi/4];

    %
    % Shape functions derivative for all elements
    %
    
    Nd_xi_all_el=repmat(Nd_xi,1,n_el);
    Nd_eta_all_el=repmat(Nd_eta,1,n_el);
    
    % components of the Jacobians: size=(1,n_int)
    J11=sum(xx_int.*Nd_xi_all_el); J12=sum(yy_int.*Nd_xi_all_el); 
    J21=sum(xx_int.*Nd_eta_all_el); J22=sum(yy_int.*Nd_eta_all_el); 
    
    % determinant of the Jacobian: size=(1,n_int)
    DET=J11.*J22 - J12.*J21;
    
    % components of the inverse to the Jacobian: size=(1,n_int)
    Jinv11 =  J22./DET; Jinv12 = -J12./DET;
    Jinv21 = -J21./DET; Jinv22 =  J11./DET;
    
    % derivatives of local basis functions w.r.t the coordinates x_1,x_2:
    % size(DPhi1)=size(DPhi2)=(n_p,n_int)
    Nd_x = repmat(Jinv11,n_n_el,1).*Nd_xi_all_el + repmat(Jinv12,n_n_el,1).*Nd_eta_all_el;
    Nd_y = repmat(Jinv21,n_n_el,1).*Nd_xi_all_el + repmat(Jinv22,n_n_el,1).*Nd_eta_all_el;

    B=cell(3,8); [B{:,:}]=deal(sparse(n_int_tot,1));
    B{1,1}=Nd_x(1,:)'; B{1,3} = Nd_x(2,:)'; B{1,5} = Nd_x(3,:)'; B{1,7} = Nd_x(4,:)';
    B{2,2}=Nd_y(1,:)'; B{2,4} = Nd_y(2,:)'; B{2,6} = Nd_y(3,:)'; B{2,8} = Nd_y(4,:)';
    B{3,1}=Nd_y(1,:)'; B{3,3} = Nd_y(2,:)'; B{3,5} = Nd_y(3,:)'; B{3,7} = Nd_y(4,:)';
    B{3,2}=Nd_x(1,:)'; B{3,4} = Nd_x(2,:)'; B{3,6} = Nd_x(3,:)'; B{3,8} = Nd_x(4,:)';

    
    % J1=cell(2,2); [J1{:,:}]=deal(sparse(nel,1));
    % J1{1,1} = xx*Nd_xi';
    % J1{2,1} = xx*Nd_eta';
    % J1{1,2} = yy*Nd_xi';
    % J1{2,2} = yy*Nd_eta';
    % 
    % detJ =J1{1,1}.*J1{2,2} - J1{1,2}.*J1{2,1};
    % 
    % tInvJ1=cell(2,2); [tInvJ1{:,:}]=deal(sparse(nel,1));
    % 
    % tInvJ1{1,1} = J1{2,2}./detJ;
    % tInvJ1{1,2} = -J1{1,2}./detJ;
    % tInvJ1{2,1} = -J1{2,1}./detJ;
    % tInvJ1{2,2} = J1{1,1}./detJ;
    % 
    % B3=zeros(4,8);
    % B3(1:2,1:2:end)=[Nd_xi; Nd_eta];
    % B3(3:4,2:2:end)=[Nd_xi; Nd_eta];
    % 
    % B=cell(3,8); [B{:,:}]=deal(sparse(nel,1));
    % 
    % B{1,1} = tInvJ1{1,1}*B3(1,1) + tInvJ1{1,2}*B3(2,1);
    % B{1,3} = tInvJ1{1,1}*B3(1,3) + tInvJ1{1,2}*B3(2,3);
    % B{1,5} = tInvJ1{1,1}*B3(1,5) + tInvJ1{1,2}*B3(2,5);
    % B{1,7} = tInvJ1{1,1}*B3(1,7) + tInvJ1{1,2}*B3(2,7);
    % 
    % B{2,2} = tInvJ1{2,1}*B3(3,2) + tInvJ1{2,2}*B3(4,2);
    % B{2,4} = tInvJ1{2,1}*B3(3,4) + tInvJ1{2,2}*B3(4,4);
    % B{2,6} = tInvJ1{2,1}*B3(3,6) + tInvJ1{2,2}*B3(4,6);
    % B{2,8} = tInvJ1{2,1}*B3(3,8) + tInvJ1{2,2}*B3(4,8);
    % 
    % B{3,1} = tInvJ1{2,1}*B3(1,1) + tInvJ1{2,2}*B3(2,1);
    % B{3,2} = tInvJ1{1,1}*B3(3,2) + tInvJ1{1,2}*B3(4,2);
    % B{3,3} = tInvJ1{2,1}*B3(1,3) + tInvJ1{2,2}*B3(2,3);
    % B{3,4} = tInvJ1{1,1}*B3(3,4) + tInvJ1{1,2}*B3(4,4);
    % B{3,5} = tInvJ1{2,1}*B3(1,5) + tInvJ1{2,2}*B3(2,5);
    % B{3,6} = tInvJ1{1,1}*B3(3,6) + tInvJ1{1,2}*B3(4,6);
    % B{3,7} = tInvJ1{2,1}*B3(1,7) + tInvJ1{2,2}*B3(2,7);
    % B{3,8} = tInvJ1{1,1}*B3(3,8) + tInvJ1{1,2}*B3(4,8);

end
