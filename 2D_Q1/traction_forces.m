function f_t = traction_forces(segments,coord,traction_force)

    %,f_t_int,HatP_s,DHatP1_s,WF_s
    
    % - surface is created by the reference line with coordinates:
    %               -1, 1
    % - 2-point quadrature rule, i.e., n_q_s=2
    pt = 1/sqrt(3);
    Xi_s=[-pt,pt];
    WF_s=[1,1];
    xi = Xi_s(1,:);
    HatP_s = [xi.*(xi-1)/2; xi.*(xi+1)/2; (xi+1).*(1-xi)] ;
    DHatP1_s = [ xi-1/2; xi+1/2; -2*xi];

    n_n=size(coord,2);    % number of nodes including midpoints
    n_seg=size(segments,2); % number of segments
    n_n_seg=size(segments,1); % number of nodes in one segment
    n_int_seg=2;   % number of quadrature points on a segment
    n_int_seg_all=n_seg*n_int_seg; % number of integration points on the surface

    f_t_int=traction_force'*ones(1,n_int_seg_all);
                        

%
% Jacobians and their determinants at surface integration points
%                         

  % extension of the input arrays HatP_s,DHatP1_s by replication
  % size(HatPhi_s)=size(DHatPhi1_s)=(n_p_s,n_int_s)
  DHatPhi1_s=repmat(DHatP1_s,1,n_seg);
  HatPhi_s=repmat(HatP_s,1,n_seg)  ;
  
  % coordinates of nodes defining each surface element
  % size(COORDs1)=(n_p_s,n_e_s)
  COORDs1=reshape(coord(1,segments(:)),n_n_seg,n_seg);
  
  % coordinates of nodes around each surface integration point
  % size(COORDint1)=(n_p_s,n_int_s)
  COORDint1=kron(COORDs1,ones(1,n_int_seg)); 
  
  % components of the Jacobians: size=(1,n_int_s)
  J11=sum(COORDint1.*DHatPhi1_s);
  
  % determinant of the Jacobian: size=(1,n_int_s)
  DET_s=J11;
  
  % weight coefficients: size(WEIGHT)=(1,n_int_s)
  WEIGHT_s = abs(DET_s).*repmat(WF_s,1,n_seg);
  
%
% Assembling of the vector of traction forces, size(f_V)=(2,n_n)
%

  % auxilliary values at surface integration points, 
  % size(vF1)=size(vF2)=(n_p_s,n_int_s)   
  vF1 = HatPhi_s.*(ones(n_n_seg,1)*(WEIGHT_s.*f_t_int(1,:)));    
  vF2 = HatPhi_s.*(ones(n_n_seg,1)*(WEIGHT_s.*f_t_int(2,:)));    
  % row and column indices, size(iF)=size(jF)=(n_p_s,n_int_s)   
  iF = ones(n_n_seg,n_int_seg_all);
  jF = kron(segments,ones(1,n_int_seg));
  % the asssembling by using the sparse command - values v for duplicate
  % doubles i,j are automatically added together
  f_t = [ sparse(iF(:), jF(:), vF1(:), 1, n_n);
          sparse(iF(:), jF(:), vF2(:), 1, n_n) ];  
  
 end