function [FTOTAL] = internal_forces(B,S,detJW,n_int_tot,ndof,elements)

    FINT = zeros(n_int_tot,8); 

    for i=1:24
        FINT(:,i) = B{1,i}.*S(1,:)'+B{2,i}.*S(2,:)'+B{3,i}.*S(3,:)' + ...
                    B{4,i}.*S(4,:)'+B{5,i}.*S(5,:)'+B{6,i}.*S(6,:)' ;
    end

    FTOTAL=sparse(ndof,1);

    inodes=[1 1 1 2 2 2 3 3 3 4 4 4 5 5 5 6 6 6 7 7 7 8 8 8]; 
    icomps=[1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3];

    for i=1:24
        ik=3*(inodes(i)-1)+icomps(i);it=3*(elements(inodes(i),:)-1)+icomps(i);
        FTOTAL=FTOTAL+sparse(it,1,sum(reshape(FINT(:,ik)'.*detJW,8,[]),1),ndof,1);
    end


end