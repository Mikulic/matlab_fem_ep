function [KGTOTAL] = kg_22_matrix_compute_2_work(B, detJ, CC,n_int_tot, elem, n_dof, IND_p)

n_ind_p=sum(IND_p);
KGTOTAL=sparse(n_dof,n_dof);
DIAG=sparse(n_dof,1);

inodes=[1 1 1 2 2 2 3 3 3 4 4 4 5 5 5 6 6 6 7 7 7 8 8 8]; 
icomps=[1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3];

E=cell(6,24); [E{:,:}]=deal(zeros(n_ind_p,1)); 

for i=1:6
    for j=1:24
        E{i,j}=CC{i,1}.*B{1,j}+CC{i,2}.*B{2,j}+CC{i,3}.*B{3,j}+ ... 
               CC{i,4}.*B{4,j}+CC{i,5}.*B{5,j}+CC{i,6}.*B{6,j};
    end
end

    % for i=1:24
    %     for j=1:i
    %     KK=zeros(n_int_tot,1);
    %     Kij = detJW'.*(B{1,i}.*E{1,j}+B{2,i}.*E{2,j}+B{3,i}.*E{3,j} + ...
    %                              B{4,i}.*E{4,j}+B{5,i}.*E{5,j}+B{6,i}.*E{6,j});
    %     KK(IND_p)=Kij;
    %     KELTOTAL{i,j} = KK;
    %     end
    % 
    % end

for i=1:24
    k=sparse(n_dof,n_dof);
    KKii=zeros(n_int_tot,1);
    it=3*(elem(inodes(i),:)-1)+icomps(i);
    for j=1:i-1
        KKij=zeros(n_int_tot,1);
        jt=3*(elem(inodes(j),:)-1)+icomps(j);

         Kij = detJ'.*(B{1,i}.*E{1,j}+B{2,i}.*E{2,j}+B{3,i}.*E{3,j} + ...
                         B{4,i}.*E{4,j}+B{5,i}.*E{5,j}+B{6,i}.*E{6,j});
         KKij(IND_p)=Kij;
        k=k+sparse(it,jt,sum(reshape(KKij,8,[]),1),n_dof,n_dof);
    end
    Kii = detJ'.*(B{1,i}.*E{1,i}+B{2,i}.*E{2,i}+B{3,i}.*E{3,i} + ...
                  B{4,i}.*E{4,i}+B{5,i}.*E{5,i}+B{6,i}.*E{6,i});
    KKii(IND_p)=Kii;
    DIAG=DIAG+sparse(it,1,sum(reshape(KKii,8,[]),1),n_dof,1);
    KGTOTAL = KGTOTAL + k;
    clear k;
end

KGTOTAL=KGTOTAL+KGTOTAL.'+diag(DIAG);

end