function [C,CC] = c_matrix_compute_3D_Q1(n_int_tot)

% ======================================================================
%
% Constitutive matrix computation
%
%    output: 
%      C - constitutive matrix
%                  
% =========================================================================


    E = 206900;                        % Youngs modulus
    nu = 0.29;                         % Poisson's ratio
%     C = (E/((1+nu)*(1-2*nu)))*[1-nu nu 0 ; nu 1-nu 0; 0 0 (1-2*nu)/2];
%     C1=(E/((1+nu)*(1-2*nu)))*ones(n_int_tot,1);
%     CC=cell(3,3); [CC{:,:}]=deal(sparse(n_int_tot,1));
%     CC{1,1}=C1*(1-nu); CC{2,2}=C1*(1-nu);
%     CC{1,2}=C1*nu; CC{2,1}=C1*nu;
%     CC{3,3}=C1*(1-2*nu)/2;
    mu=.5*E/(1+nu); lam=E*nu/((1+nu)*(1-2*nu));
    C=zeros(6);C(1:3,1:3)=[lam+mu lam    lam;
                           lam    lam+mu lam;
                           lam    lam    lam+mu]; C=C+mu*eye(6);  
    CC=cell(6,6); [CC{:,:}]=deal(sparse(n_int_tot,1));
    mu=mu*ones(n_int_tot,1); lam=lam*ones(n_int_tot,1);
    CC{1,1}=lam+2*mu; CC{2,2}=lam+2*mu; CC{3,3}=lam+2*mu;
    CC{4,4}=mu; CC{5,5}=mu; CC{6,6}=mu;
    CC{1,2}=lam; CC{1,3}=lam; CC{2,3}=lam;
    CC{2,1}=lam; CC{3,1}=lam; CC{3,2}=lam;
end