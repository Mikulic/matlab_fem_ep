function [segments] = traction_surfaces(nodes, tnodes)

% ======================================================================
%
% Calculation of traction forces
%
%    output: 
%      segments - segments of traction forces
%                  
%    input:
%      traction_force - intensity of traction force                       
%      nodes - coordinates of nodes 
%      tnodes - numbers of traction forces nodes
%
% =========================================================================

    nnodes=size(nodes,2);
    tnodes_coord=nodes(1:2,tnodes);
    [~,order] = sortrows(sortrows(tnodes_coord',1),2);
    tnodes=tnodes(order);

    ntnodes=length(tnodes);
    %
    segments=[tnodes(1:2:ntnodes-2); tnodes(3:2:ntnodes); tnodes(2:2:ntnodes-1);];
    % lines_xy=nodes(:,lines(:,3))'-nodes(:,lines(:,1))';
    % lines_length=sqrt(sum(lines_xy.^2,2));
    % 
    % % assembly
    % lines_force_x = lines_length*traction_force(1)/2;
    % lines_force_y = lines_length*traction_force(2)/2;
    % 
    % forces=full(sparse(lines(:,1)*2-1,1,lines_force_x,nnodes*2,1) + ... 
    %         sparse(lines(:,2)*2-1,1,lines_force_x,nnodes*2,1)+ ...
    %         sparse(lines(:,1)*2,1,lines_force_y,nnodes*2,1) + ... 
    %         sparse(lines(:,2)*2,1,lines_force_y,nnodes*2,1));
    % forces=reshape(forces,2,nnodes);
end